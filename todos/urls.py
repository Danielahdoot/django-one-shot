from django.urls import path
from todos.views import todo_list_list, todo_item_list


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_item_list, name="todo_list_detail"),
]
