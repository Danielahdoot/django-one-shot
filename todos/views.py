from django.shortcuts import render, get_list_or_404, redirect
from todos.models import TodoList, TodoForm

# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/list.html", context)


def todo_item_list(request, id):
    todo_list = get_list_or_404(TodoList, id=id)
    context = {
        "item_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm
        context = {
            "form": form,
        }
    return render(request, "todos", context)
